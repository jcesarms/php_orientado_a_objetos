<?php  
//declaracion de clase token
	class token{
		//declaracion de atributos
		private $nombre;
		private $token;
        private $DesdeLetra = "A";
        private $HastaLetra = "Z";

		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
            $contador = 0;
            $contrasania ='';
            while($contador < 4){
                $contrasania .= chr(rand(ord($this->DesdeLetra), ord($this->HastaLetra)));
                $contador++;
            }
            $this->token = $contrasania;
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' esta es tu contraseña: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='La contraseña "' .$this->token.'" ha sido destruida';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>