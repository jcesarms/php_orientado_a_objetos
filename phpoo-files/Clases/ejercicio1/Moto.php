<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto{
	//declaracion de propiedades
	public $modelo;
	public $marca;
}
$mensajeServidor2 = '';
//crea aqui la instancia o el objeto de la clase Moto
$Moto1 = new Moto();

 if ( !empty($_POST)){
 	 // recibe aqui los valores mandados por post y arma el mensaje para front 
	$Moto1->marca = $_POST['marca'];
	$Moto1->modelo = $_POST['modelo'];

	$mensajeServidor2 = 'El servidor dice que la marca de la moto es ' . $Moto1->marca .' y el modelo '.$Moto1->modelo;
 }
