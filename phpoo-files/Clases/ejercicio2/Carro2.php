<?php
//creación de la clase carro
class Carro2
{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $resultado;

	//declaracion del método verificación
	public function verificacion($cadena)
	{
		$anio = intval(substr($cadena, 0, -2));
		if ($anio < 1990) {
			$this->resultado ='No';
		} else if ($anio > 1990 && $anio < 2010) {
			$this->resultado = 'Revision';
		} else {
			$this->resultado = 'Si';
		}
	}
	//Obtenemos el resultado de verificación
	public function getVerificacion()
	{
		return $this->resultado;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)) {
	$Carro1->color = $_POST['color'];
	$Carro1->modelo = $_POST['modelo'];
	$Carro1->verificacion($_POST['anio']);
}
